---
sidebar_position: 1
---

# Вводная часть

В данном разделе представлена обзорная информация о контроле версий, Git, CI и CD.

## Презентация

1. [Git для самых маленьких](https://gitlab.com/iu5edu/cpp-course-sem2/textbook/-/blob/main/src/git-for-youngest.pdf)
