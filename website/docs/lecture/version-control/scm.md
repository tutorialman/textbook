---
sidebar_position: 2
---

# Управление исходным кодом

**Управление исходным кодом** (англ. source control management, SCM) используется для отслеживания изменений в *репозитории исходного кода*. SCM отслеживает текущую историю изменений в базе кода и помогает разрешать конфликты при объединении обновлений от нескольких участников.

**Репозиторий** — место, где хранятся и поддерживаются какие-либо данные. Чаще всего данные в репозитории хранятся в виде файлов, доступных для дальнейшего распространения по сети. Также под репозиторием понимается каталог файловой системы, в котором могут находиться файлы журналов конфигураций и операций, выполняемых над репозиторием, а также сами контролируемые файлы.

*Git-репозиторий (источник: [learn.microsoft.com](https://learn.microsoft.com/ru-ru/devops/develop/git/set-up-a-git-repository))*:

![Git-репозиторий](images/git-repositories.png)

По мере роста количества строк кода и количества участников программных проектов растут и затраты на коммуникационные издержки, а также сложность управления. SCM является важнейшим инструментом для облегчения организационной нагрузки, связанной с растущими затратами на разработку.

## Важность инструментов управления исходным кодом

Когда несколько разработчиков работают в рамках общей кодовой базы, внесение изменений в общий фрагмент кода является обычным делом. Отдельные разработчики могут работать над, казалось бы, изолированной функцией, однако эта функция может использовать общий модуль кода. Поэтому разработчик №1, работающий над функцией №1, может внести некоторые правки и позже выяснить, что разработчик №2, работающий над функцией №2, имеет конфликтующие правки.

*В интересах кого SCM работает (источник [atlassian.com/git](https://www.atlassian.com/git/tutorials/why-git))*:

![SCM и компания](images/scm-and-company.svg)

До принятия SCM это был кошмарный сценарий. Разработчики будут редактировать текстовые файлы напрямую и перемещать их в удаленные места с помощью FTP или других протоколов. Разработчик №1 внесет изменения, а разработчик №2 неосознанно сохранит работу разработчика №1 и уничтожит изменения. Роль SCM как механизма защиты от этого конкретного сценария известна как контроль версий.

SCM внедрила меры контроля версий для предотвращения потери работы из-за перезаписи конфликта. Эти меры предосторожности работают путем отслеживания изменений от каждого отдельного разработчика, выявления конфликтных областей и предотвращения перезаписи. Затем SCM сообщит об этих конфликтных моментах разработчикам, чтобы они могли безопасно просмотреть и устранить.

*Взаимодействие в команде разработки (источник [atlassian.com/git](https://www.atlassian.com/git/tutorials/why-git))*:

![SCM и компания](images/sharing-code.svg)

Этот основополагающий механизм предотвращения конфликтов имеет побочный эффект, заключающийся в обеспечении пассивной коммуникации для команды разработчиков. Затем команда может отслеживать и обсуждать текущую работу, которую отслеживает SCM. SCM отслеживает всю историю изменений в базе кода. Это позволяет разработчикам изучать и просматривать правки, которые могли привести к ошибкам или регрессиям.

## Преимущества управления исходным кодом

В дополнение к контролю версий SCM предоставляет набор других полезных функций, которые делают совместную разработку кода более удобной для пользователя. Как только SCM начинает отслеживать все изменения в проекте с течением времени, создается подробная историческая запись жизненного цикла проекта. Эта историческая запись затем может быть использована для ‘отмены’ изменений в кодовой базе. SCM может мгновенно вернуть кодовую базу к предыдущему моменту времени. Это чрезвычайно важно для предотвращения регрессий при обновлениях и устранения ошибок.

1. Архив SCM всех изменений за время существования проекта обеспечивает ценный **учет примечаний к выпускной версии проекта**. Чистый и поддерживаемый журнал истории SCM можно взаимозаменяемо использовать в качестве примечаний к выпуску. Это обеспечивает понимание и прозрачность хода выполнения проекта, которыми можно поделиться с конечными пользователями или командами, не занимающимися разработкой.
2. SCM **сокращает коммуникационные издержки команды и увеличит скорость выпуска**. Без SCM разработка идет медленнее, потому что участникам приходится прилагать дополнительные усилия для планирования непересекающейся последовательности разработки для выпуска. С помощью SCM разработчики могут независимо работать над отдельными разделами разработки функций, в конечном итоге объединяя их вместе.

*Коммуникация в команде разработки (источник [atlassian.com/git](https://www.atlassian.com/git/tutorials/why-git))*:

![SCM и компания](images/communication.svg)

В целом SCM - это огромная помощь инженерным командам, которая позволит снизить затраты на разработку, позволяя инженерным ресурсам работать более эффективно. SCM - это обязательное условие в современную эпоху разработки программного обеспечения. Профессиональные команды используют контроль версий, и ваша команда тоже должна это делать.

## Вывод

SCM - бесценный инструмент для современной разработки программного обеспечения. Лучшие команды разработчиков программного обеспечения используют SCM, и ваша команда тоже должна. SCM очень легко настроить в новом проекте, а отдача от инвестиций высока. Atlassian предлагает одни из лучших в мире инструментов интеграции SCM, которые помогут вам начать работу.

## Атрибуция

При подготовке статьи использован материал:

- [atlassian.com/git/tutorials](https://www.atlassian.com/git/tutorials/)
- [wiki.miem.hse.ru/docs](https://wiki.miem.hse.ru/docs/miem-digital/git)
