---
sidebar_position: 2
---

# Видеозапись лекции

Материал данного руководства использовался для подготовки лекции курса "Автоматизация развертывания и эксплуатации программного обеспечения" в МГТУ им. Н.Э. Баумана 19.09.2022.

<iframe src="https://vk.com/video_ext.php?oid=-215612996&id=456239017&hash=a1e09ed0f7391440&hd=2" width="853" height="480" allow="autoplay; encrypted-media; fullscreen; picture-in-picture;" frameborder="0" allowfullscreen></iframe>