---
sidebar_position: 10
---

# Файловая система Git и команды

Как уже упоминалось [ранее](../distinctive-features.md), у Git есть три основных состояния, в которых могут находиться ваши файлы: *изменён* (modified), *индексирован* (staged) и *зафиксирован* (committed).

Давайте посмотрим как в эти состояния можно переходить с помощью команд.

*Изменение состояний с помощью команд (источник: [tproger.ru](https://tproger.ru/translations/beginner-git-cheatsheet/))*:

![Изменение состояний](images/areas-and-commands.jpeg)

На иллюстрации:

- **рабочая директория** (файловая система вашего компьютера);
- **область подготовленных файлов** (staging area, хранит содержание следующего коммита);
- **HEAD** (последний коммит в репозитории).

Все основные команды по работе с файлами сводятся к пониманию того, как Git управляет этими тремя разделами. Существует распространённое заблуждение, что область подготовленных файлов только хранит изменения. Лучше думать об этих трёх разделах как об отдельных файловых системах, каждая из которых содержит свои копии файлов.

Предположим, что у нас в локальном репозитории:

![Пример 1](images/areas-and-commands/example-1.png)

:::note Команда `git reset`

Сброс текущей `HEAD` в заданное состояние

Документация [здесь](https://git-scm.com/docs/git-reset).
:::

Выполним команду:

```bash
git reset HEAD^ --soft
```

Статус репозитория:

![Пример 2](images/areas-and-commands/example-2.png)

Добавим файл в индекс:

```bash
git add README
```

Статус репозитория:

![Пример 3](images/areas-and-commands/example-3.png)

Отменим готовность файла:

```bash
git reset HEAD README 
```

Статус репозитория:

![Пример 4](images/areas-and-commands/example-4.png)

```bash
git add README
```

:::note Команда `git checkout`

Переключение ветвей или восстановление файлов рабочего дерева.

Документация [здесь](https://git-scm.com/docs/git-checkout).
:::

Добавим файл `test.md`:

```bash
echo "re" > test.md
```

Статус репозитория:

![Пример 5](images/areas-and-commands/example-5.png)

Выполним `checkout`:

```bash
git checkout README
```

![Пример 6](images/areas-and-commands/example-6.png)

`checkout` восстановил историческую ревизию файла `README`. А именно в выполненной команде файл восстановился до состояния, куда указывал `HEAD`. Фактически отменяются все локальные изменения, которые произошли с тех пор.

Иными словами `git checkout <файл>` без `HEAD` перезаписывает версию файла в рабочей директории версией в области подготовленных файлов, то есть отменяет изменения с момента последней подготовленной версии.

Если вы хотите восстановить определенную более раннюю ревизию, вы можете указать хэш SHA-1 этой ревизии.

Статус до внесения изменений:

![Пример 7](images/areas-and-commands/example-7.png)

Выполним `checkout` с указанием хэш SHA-1 первого коммита:

```bash
git checkout 02c9b2 README
```

Статус репозитория:

![Пример 8](images/areas-and-commands/example-8.png)

:::note Команда `git diff`

показывает изменения между коммит, коммитом и рабочим деревом и т.д.

Документация [здесь](https://git-scm.com/docs/git-diff).
:::

Чтобы посмотреть сами изменения, а не изменённые файлы, можно использовать следующие команды:

- `git diff` — сравнение рабочей директории с областью подготовленных файлов;
- `git diff --staged` — сравнение области подготовленных файлов с HEAD.

Посмотрим изменения, которые были добавлены в индекс:

```bash
git diff --staged
```

Покажет изменения:

```bash
index 07c3486..78535a3 100644
--- a/README
+++ b/README
@@ -1 +1 @@
-There is a small exception to this, which is the detached HEAD. A detached HEAD is the situation you end up in whenever you check out a commit (or tag) instead of a branch. In this case, you have to imagine this as a temporary branch without a name; so instead of having a named branch reference, we only have HEAD. It will still allow you to make commits (which will update HEAD), so the above short definition is still true if you think of a detached HEAD as a temporary branch without a name.
\ No newline at end of file
+My Project 2
```

В более наглядном виде:

![Пример 9](images/areas-and-commands/example-9.png)

Подробнее о `git diff` в статье на [pingvinus.ru](https://pingvinus.ru/git/1605).

## Атрибуция

При подготовке статьи использован материал:

- [tproger.ru](https://tproger.ru/translations/beginner-git-cheatsheet/#part2)
