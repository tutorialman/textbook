---
sidebar_position: 2
---

# Настройки Git

В зависимости от области действия и места хранения в Git cуществуют 3 типа настроек:

- **Системные**. Представляют собой настройки на уровне всей системы, то есть они распространяются на всех пользователей. Файл с этими настройками хранится по следующему пути: `C:\Program Files\Git\etc\gitconfig` для Windows и `/etc/gitconfig` для пользователей Linux/MacOS.
- **Глобальные**. Эти настройки одинаковы для всех репозиториев, созданных под вашим пользователем. Среди них есть, например, имя ветки по умолчанию. Файл с этими параметрами хранятся по следующему адресу: `C:/User/<имя пользователя>/.gitconfig` в windows, или `~/.gitconfig` в Unix системах.
- **Локальные**. Это настройки на уровне репозитория, они не будут применяться к другим вашим проектам. Эти параметры хранятся в каждом вашем репозитории по адресу: `.git/config`.

*Области действия настроек Git (источник: [smartiqa.ru/courses/git](https://smartiqa.ru/courses/git/lesson-2))*:

![Области действия настроек Git](images/git-config.png)

Изменить настройки Git можно двумя способами:

- **Отредактировать файл** `gitconfig` (на уровне системы) или `.gitconfig` (глобально) или `.git/config` (на уровне репозитория) напрямую, то есть используя текстовый редактор.
- **Воспользоваться утилитой `git config`**. Кроме того, с помощью этой утилиты можно посмотреть значение соответствующего параметра.

:::note Команда `git config`

Устанавливает значение соответствующего параметра в конфигурации Git.

Документация [здесь](https://git-scm.com/docs/git-config).
:::

Давайте попробуем задать имя пользователя глобально. Воспользуемся утилитой `git config` с параметром --global:

```bash
git config --global user.name "Bauman Student"
git config --global user.email bauman@example.com
```

Посмотреть заданные переменные можно:

```bash
git config --global user.name
git config --global user.email
```

Файл `$HOME/.gitconfig` с заданным пользователем:

```bash
[user]
        name = Bauman Student
        email = bauman@example.com
[filter "lfs"]
        clean = git-lfs clean -- %f
        smudge = git-lfs smudge -- %f
        process = git-lfs filter-process
        required = true
```

Как видно поля `user.name` и `user.email` действительно стали такими, какими мы их задали.

:::note Команда `git config --list --show-origin`

Показывает все заданные переменные списком (`--list`) и откуда они наследованы (`--show-origin`).
:::

## Атрибуция

При подготовке статьи использован материал:

- [git-scm.com/docs](https://git-scm.com/docs/)
- [smartiqa.ru/courses/git](https://smartiqa.ru/courses/git)
