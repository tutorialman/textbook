---
sidebar_position: 2
---

# GitHub Flow

**GitHub flow** - это облегченный рабочий процесс на основе ветвей. GitHub flow полезен для всех, а не только для разработчиков (по утверждению ~~корпорации Microsoft~~ компании GitHub).

## Что такое GitHub Flow?

*Иллюстрация процесса (источник: [docs.github.com](https://docs.github.com/en/get-started/quickstart/github-flow))*:

![GitHub Flow](images/github-flow.png)

Более подробно в статьях:

- [habr.com](https://habr.com/ru/post/346066/)
- [javarush.ru](https://javarush.ru/groups/posts/2693-komandnaja-rabota-bez-putanicih-razbiraem-strategii-vetvlenija-v-gite)
