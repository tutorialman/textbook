---
sidebar_position: 1
---

# Gitflow

**Gitflow** - это устаревший рабочий процесс Git, который изначально был новаторской и новой стратегией для управления ветвями Git. Популярность Gitflow упала в пользу рабочих процессов на основе магистралей, которые в настоящее время считаются лучшими практиками для современной непрерывной разработки программного обеспечения и практики DevOps. Gitflow также может быть сложным в использовании с CI/CD.

## Что такое Gitflow?

*Иллюстрация процесса (источник: [fuzeservers.ru](https://fuzeservers.ru/programming/fatalnyj-ne-repozitorij-git-ili-luboj-iz-roditelskih-katalogov-iz-statusa-git.html))*:

![Gitflow](images/gitflow.png)

Более подробно в статье на [atlassian.com/git](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow).

## Атрибуция

При подготовке статьи использован материал:

- [atlassian.com/git](https://www.atlassian.com/git)
