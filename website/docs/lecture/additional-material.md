---
sidebar_position: 7
---

# Дополнительный материал

## О Git

### Книги

1. Фишерман Л. В. Практическое руководство. Управление и контроль версий в разработке программного обеспечения

### Курсы

1. [GitKraken: Learn Git](https://www.youtube.com/playlist?list=PLe6EXFvnTV7-_41SpakZoTIYCgX4aMTdU)
2. [Smartiqa: Работа с Git](https://smartiqa.ru/courses/git)

### Статьи

2. [Введение в Git: от установки до основных команд](https://tproger.ru/translations/beginner-git-cheatsheet/)
3. [Git за полчаса: руководство для начинающих](https://proglib.io/p/git-for-half-an-hour)
4. [Что такое GIT простым языком? Как работает, основные команды GIT](https://www.youtube.com/watch?v=buygCuSqBsA)
