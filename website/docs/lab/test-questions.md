---
sidebar_position: 4
---

# Контрольные вопросы

1. Базовые понятия методологии разработки TDD. Материал для подготовки [№1](https://habr.com/ru/company/ruvds/blog/450316/).
2. Пирамида автоматизации тестирования. Материал для подготовки [№1](https://hashnets.com/разновидности-юнит-тестов-unit-testing/) и [№2](https://practicum.yandex.ru/blog/chto-takoe-modulnoe-yunit-testirovanie/).
3. 7 принципов тестирования (в соответствии с ISTQB). Материал для подготовки [№1](https://habr.com/ru/post/549054/), [№2](https://www.boxuk.com/insight/the-seven-principles-of-testing/) и [№3](https://www.etestware.com/7-principles-of-testing/).
4. Что такое GitHub Flow? Материал для подготовки [№1](https://iu5edu.gitlab.io/git-ci-cd/textbook/docs/workflows/github-flow).
5. Что такое GitLab CI/CD? Концепции CI/CD. Материал для подготовки [№1](https://docs.gitlab.com/ee/ci/quick_start/) и [№2](https://docs.gitlab.com/ee/ci/introduction/).
6. Что такое GitFlow? Материал для подготовки [№1](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) и [№2](https://proglib.io/p/git-github-gitflow).
7. Как команды используют краткосрочные feature-ветки и пулл-реквесты. Материал для подготовки [№1](https://tproger.ru/translations/benefits-of-trunk-based-development/).
8. Типы систем контроля версий. Материал для подготовки [№1](https://tproger.ru/translations/difference-between-git-and-github/).
