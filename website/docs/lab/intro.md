---
sidebar_position: 1
---

# Вводная часть

Лабораторная работа имеет две части:

1. [Контроль версий. Git](git/intro.md)
2. [CI и CD](ci-cd/intro.md)
