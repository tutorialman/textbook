---
sidebar_position: 3
---

# Получение репозитория

Получение репозитория осуществляется через систему [iu5edu.ru](https://iu5edu.ru/). Для работы в системе необходимо иметь аккаунт на платформе [Технополиса МГТУ им. Н. Э. Баумана](https://sso.bmstu.com/). Регистрация осуществляется согласно инструкциям на сайте технополиса. Электронную почту рекомендуется указывать небауманскую, так как у аккаунта нельзя сменить почту. При переходе из бакалавриата в магистратуру бауманская почта меняется.

Также необходимо иметь аккаунт на платформе [bmstu.codes](https://bmstu.codes/). Авторизация происходит через аккаунт технополиса:

![Авторизация bmstu.codes](images/authorization-bmstu-codes.png)

:::danger

При регистрации в системе [iu5edu.ru](https://iu5edu.ru/) необходимо указать программу вашего обучения (`бакалавр` или `магистр`) и семестр обучения.

:::

Далее можно перейти к получению репозитория в системе [iu5edu.ru](https://iu5edu.ru/).

На платформе выбирается курс `Контроль версий. Git. CI и CD`. В названии курса могут присутствовать год обучения и данные о том, в рамках какой дисциплины дается курс.

:::info

По умолчанию выдается роль `Developer`. Если необходима роль `Maintainer`, то обратитесь к преподавателю.

:::

## Пошаговая инструкция

Авторизация в системе [iu5edu.ru](https://iu5edu.ru/):

![Авторизация в системе](images/get-repository-step1.png)

Авторизация на платформе [bmstu.codes](https://bmstu.codes/):

![Авторизация вна платформе](images/get-repository-step2.png)

Разрешение авторизации через профиль:

![Разрешение авторизации через профиль](images/get-repository-step3.png)

Регистрация в системе:

![Регистрация](images/get-repository-step4.png)

Далее нажать на кнопку `Доступные курсы`. После чего нажать кнопку `Приступить`:

![Выбор курса](images/get-repository-step5.png)

В результате вы получите репозиторий:

![Результат](images/get-repository-result.png)
