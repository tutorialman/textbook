---
sidebar_position: 10
---

# Cherry-pick и Git commit

:::info

Материал статьи основан на ["Cherry-pick a Git commit"](https://docs.gitlab.com/ee/topics/git/cherry_picking.html)

:::

В Git вы можете выбрать фиксацию (набор изменений) из существующей ветки и применить эти изменения к другой ветке. Cherry-pick может помочь вам:

- Исправить ошибки переноса из ветки по умолчанию в ветки предыдущего выпуска.
- Копировать изменений из fork в вышестоящий репозиторий (upstream repository).

Вы можете выбирать вишневые коммиты из командной строки. В пользовательском интерфейсе GitLab вы также можете:

- Cherry-pick [все изменения из запроса на слияние](https://docs.gitlab.com/ee/user/project/merge_requests/cherry_pick_changes.html#cherry-pick-all-changes-from-a-merge-request).
- Cherry-pick [один коммит](https://docs.gitlab.com/ee/user/project/merge_requests/cherry_pick_changes.html#cherry-pick-a-single-commit).
- Cherry-pick [из fork в вышестоящий репозиторий](https://docs.gitlab.com/ee/user/project/merge_requests/cherry_pick_changes.html#cherry-pick-into-a-project).

В данной лабораторной предлагается Cherry-pick осуществить из командной строки локально.

## Cherry-pick из командной строки

Приведем пример того, как делается Cherry-pick коммита из ветки по умолчанию (`master`) в другую ветку (`stable`):

1. Создадим ветку `stable` от метки `HEAD` ветки `master`.
2. Проверьте ветку по умолчанию, затем проверьте новую stable ветка, основанная на нем:

    ```bash
    git checkout master
    git checkout -b stable
    ```

3. Вернитесь к ветке по умолчанию:

    ```bash
    git checkout master
    ```

4. Внесите свои изменения, затем зафиксируйте их:

    ```bash
    git add calculator.py
    git commit -m 'Навали новых багов в calculator.py'
    ```

5. Отображение журнала коммитов:

    ```bash
    $ git log

    commit a7e1a4a3258c2bed363330863265d04000d9cfe4 (HEAD -> master, origin/master, origin/HEAD)
    Author: Dmitry Aladin <anon.digriz@gmail.com>
    Date:   Thu Oct 20 05:05:01 2022 +0300

        Навалили новых багов

    commit fb1926085a697430c3917d9a9ce0ecb5cd5b0972 (stable)
    Merge: 18a05b7 2312b80
    Author: Dmitry Aladin <anon.digriz@gmail.com>
    Date:   Thu Oct 20 04:46:49 2022 +0300

        Merge branch 'patch1'
    ```

6. Определите последний совершенный commit и скопируйте его идентификатор. Эта информация представляет собой SHA (безопасный алгоритм хэширования) коммита. SHA - это уникальный идентификатор для этого коммита, и он понадобится вам на следующем шаге.
7. Теперь, когда вы знаете SHA, ознакомьтесь с `stable` снова переход:

    ```bash
    git checkout stable
    ```

8. Cherry-pick коммита `SHA` в ветвь `stable` :

    ```bash
    git cherry-pick <SHA>
    ```

9. Результат Cherry-pick может быть следующим:

    ![Результат Cherry-pick](images/git-cherry-pick-result.png)
