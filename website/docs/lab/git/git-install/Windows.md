---
sidebar_position: 1
---

# Установка Git на Windows

:::info

Статья основана на ["Install Git"](https://www.atlassian.com/git/tutorials/install-git) и ["Download for Windows"](https://git-scm.com/download/win).

:::

## Шаги установки

1. Загрузите последнюю версию установщика [Git для Windows](https://git-scm.com/download/win).
2. После успешного запуска программы установки вы должны увидеть экран мастера настройки Git. Следуйте инструкциям `Next` и `Finish`, чтобы завершить установку. Параметры по умолчанию довольно понятны для большинства пользователей.
3. Настройте имя пользователя и адрес электронной почты в Git, как показано в статье ["Настройки Git"](../../../lecture/git/basics/config.md) из данного руководства.
