---
sidebar_position: 2
---

# Установка Git на Linux

:::info

Статья основана на ["Install Git"](https://www.atlassian.com/git/tutorials/install-git) и ["Download for Linux and Unix"](https://git-scm.com/download/linux).

:::

Возможно, в вашем дистрибутиве git уже установлен. Чтобы узнать, откройте терминал и введите

```bash
git --version
```

Проще всего установить Git в Linux, используя предпочтительный менеджер пакетов вашего дистрибутива Linux. Если вам требуется более свежая версия Git или он по какой-то причине отсутствует в вашей операционной системе, то следуйте указаниям ниже.

## Шаги установки

1. Установите Git согласно инструкциям, указанные в статье ["Download for Linux and Unix"](https://git-scm.com/download/linux).
2. Настройте имя пользователя и адрес электронной почты в Git, как показано в статье ["Настройки Git"](../../../lecture/git/basics/config.md) из данного руководства.
