---
sidebar_position: 9
---

# Третья часть изменений

:::info

Материал статьи основан на ["Laboratory work II"](https://github.com/tp-labs/lab02)

:::

В этой части работы предлагается:

1. Создать новую локальную ветку `patch1`.
2. В этой ветке добавить комментарии к коду в файле `calculator.py`. Например, так:

    ```python
    import math

    # Получить площадь прямоугольного треугольника
    def get_area(a, b):
        return a * b

    # Рассчитать гипотенузу
    def get_hypotenuse(a, b):
        return math.sqrt(math.pow(a, 3) + math.pow(b, 3))

    if __name__ == "__main__":
        print("Введите a:")
        a = int(input())
        print("Введите b:")
        b = int(input())
        print("c =", get_hypotenuse(a, b))
        print("S =", get_area(a, b))

    ```

3. Зафиксируйте и отправьте изменения в удалённый репозиторий.
4. Перейдите в локальную ветку `master`. В этой ветке измените порядок функций `calculator.py`. Например, так:

    ```python
    import math

    def get_hypotenuse(a, b):
        return math.sqrt(math.pow(a, 3) + math.pow(b, 3))

    def get_area(a, b):
        return a * b

    if __name__ == "__main__":
        print("Введите a:")
        a = int(input())
        print("Введите b:")
        b = int(input())
        print("c =", get_hypotenuse(a, b))
        print("S =", get_area(a, b))
    ```

5. Зафиксируйте и отправьте изменения в удалённый репозиторий.
6. Создайте запрос на слияние.
7. Убедитесь в наличии конфликта:

    ![Конфликт слияния](images/merge-conflict.png)

8. Проверьте, просмотрите и объедините локально (см. [Работа с ветками](../../lecture/git/basics/branches.md)). Подсказку о том, что делать, можно посмотреть в GitLab, нажав на кнопку `Merge locally`:

    ![Слияние локально с конфликтом](images/merge-locally-with-conflict.png)

9. Зафиксируйте и отправьте изменения в удалённый репозиторий.
10. Убедиться, что открытый ранее запрос на слияния осуществлен:

    ![Слияние веток](images/merge-request-with-conflict-result.png)

11. Просмотреть текущий граф в удаленном репозитории:

    ![Граф репозитория после слияния](images/merge-request-with-conflict-graph.png)

Итоговый файл `calculator.py`, который получился для демонстрации:

```python
import math


# Рассчитать гипотенузу
def get_hypotenuse(a, b):
    return math.sqrt(math.pow(a, 3) + math.pow(b, 3))

# Получить площадь прямоугольного треугольника
def get_area(a, b):
    return a * b


if __name__ == "__main__":
    print("Введите a:")
    a = int(input())
    print("Введите b:")
    b = int(input())
    print("c =", get_hypotenuse(a, b))
    print("S =", get_area(a, b))

```
