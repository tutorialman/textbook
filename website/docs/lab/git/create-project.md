---
sidebar_position: 5
---

# Создание проекта

В данной статье мы создадим наш тестовый проект. Наш проект будет на [Python](https://www.python.org/). Для иллюстрации в данном репозитории используется [Visual Studio Code](https://code.visualstudio.com/). Как использовать Python и Visual Studio Code показано в [статье](https://code.visualstudio.com/docs/languages/python).

При работе над проектом не забудьте использовать изолированную среду Python. С помощью [Virtualenv](https://virtualenv.pypa.io/en/latest/index.html) создадим ее в папке проекта:

```python
python3 -m venv env
```

Если мы создали ее в папке проекта, то не забываем исключить папку со средой из управления системой контроля версий. Для этого в корне репозитория создается файл `.gitignore` с необходимым паттерном для игнорирования:

```text
env
```

Если вы используете IDE, то не забывайте добавить в игнорируемые служебные объекты. Например, Visual Studio Code:

```text
env
.vscode
__pycache__
```

В корне репозитория создадим файл `hypotenuse.py`:

```python
import math

def get_hypotenuse(a, b):
    return math.sqrt(math.pow(a, 3) + math.pow(b, 3))

if __name__ == "__main__":
    a = 1
    b = 2
    print(get_hypotenuse(a,b))
```

Ошибка в коде допущена специально.

В результате в репозитории получиться следующая структура файлов и папок:

```bash
variant-X-user-Y
├── hypotenuse.py
├── README.md
└── .gitignore
```
