---
sidebar_position: 2
---

# Добавление модульных тестов

:::info

Основано на статье ["Тестируем на Python: unittest и pytest. Инструкция для начинающих"](https://tproger.ru/articles/testiruem-na-python-unittest-i-pytest-instrukcija-dlja-nachinajushhih/).

:::

Добавим в проект модульное тестирование. Для этого мы добавив файл `calculator_tests.py` в корень репозитория со следующим содержимым:

```python
import unittest

from calculator import get_hypotenuse, get_area


class CalculatingHypotenuseTestCase(unittest.TestCase):
    def test_case_1(self):
        res = get_hypotenuse(3, 4)
        self.assertEqual(res, 5)

    def test_case_2(self):
        res = get_hypotenuse(5, 12)
        self.assertEqual(res, 13)

    def test_case_3(self):
        res = get_hypotenuse(8, 15)
        self.assertEqual(res, 17)

class CalculatingAreaTestCase(unittest.TestCase):
    def test_case_1(self):
        res = get_area(3, 4)
        self.assertEqual(res, 6)

    def test_case_2(self):
        res = get_area(5, 12)
        self.assertEqual(res, 30)

    def test_case_3(self):
        res = get_area(8, 15)
        self.assertEqual(res, 60)
```

Запускается тестирования с помощью команды:

```bash
python -m unittest calculator_tests.py
```

Пока тесты не проходят, но это нам и нужно!

:::danger

Не забудьте зафиксировать изменения и отправить их в удаленный репозиторий!

:::
