---
sidebar_position: 4
---

# Использование pytest в Gitlab CI

Внесем изменения `.gitlab-ci.yml`, чтобы мы могли использовать pytest:

```yaml
# This file is a template, and might need editing before it works on your project.
# Official language image. Look for the different tagged releases at:
# https://hub.docker.com/r/library/python/tags/
image: "python:3.10.3"

# команды для запуска в контейнере Docker перед запуском каждого задания.
before_script:
  - python --version
  - pip install -r requirements.txt

# различные этапы в конвейере
stages:
  - Static Analysis
  - Test

# определяет задание в статическом анализе
pylint:
  stage: Static Analysis
  script:
    - pylint -d C0301 calculator.py

pytest:
  stage: Test
  script:
    - pytest ./tests/*py --junitxml=${CI_PROJECT_DIR}/rspec.xml -s
  artifacts:
    when: always
    paths:
      - rspec.xml
    reports:
      junit: rspec.xml

```

pytest покажет название тестового примера и результат:

![Провал задачи unittest](images/pipeline-pytest-failed.png)

Тест все также провалился, но теперь с отложенной светомузыкой (в запросах на слиянии теперь мы более интересную картину увидим).

Почему мы воспользовались иным средством тестирования? Теперь мы можем использовать [Unit test reports](https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html). Благодаря тому, что pytest из коробки поддерживает [JUnit report format XML files](https://www.ibm.com/docs/en/developer-for-zos/14.1.0?topic=formats-junit-xml-format), то в  GitLab в запросе на слиянии мы сможем увидеть отчет о тестированнии отчет по запросу на слияние. Это позволит проще и быстрее идентифицировать сбой без необходимости проверять весь журнал.

Также мы задаче явно указали, что `rspec.xml` является артефактом задания. Его не мешало бы сохранять, причем всегда. Плюс мы также пометили, что этот артефакт относится к отчету о тестировании.

Сделаем более умнее наш конвейер.
