---
sidebar_position: 5
---

# Настройка условий исполнения задач

:::info

Основано на статье ["Choose when to run jobs"](https://docs.gitlab.com/ee/ci/jobs/job_control.html).

:::

В GitLab используется `rules` для включения или исключения заданий в конвейерах.

Правила оцениваются по порядку до первого совпадения. Когда найдено совпадение, задание либо включается, либо исключается из конвейера, в зависимости от конфигурации. Смотрите `rules` Ссылку для получения более подробной информации.

Давайте явно укажем, что наши проверки кода необходимо выполнять только при запросах на слияние в ветку по умолчанию:

```yaml
# This file is a template, and might need editing before it works on your project.
# Official language image. Look for the different tagged releases at:
# https://hub.docker.com/r/library/python/tags/
image: "python:3.10.3"

# команды для запуска в контейнере Docker перед запуском каждого задания.
before_script:
  - python --version
  - pip install -r requirements.txt

# различные этапы в конвейере
stages:
  - Static Analysis
  - Test

# определяет задание в статическом анализе
pylint:
  stage: Static Analysis
  rules:
    - if: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH
  script:
    - pylint -d C0301 calculator.py

pytest:
  stage: Test
  rules:
  - if: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH
  script:
    - pytest ./tests/*py --junitxml=${CI_PROJECT_DIR}/rspec.xml -s
  artifacts:
    when: always
    paths:
      - rspec.xml
    reports:
      junit: rspec.xml

```

Кстати, можно заметить что тут стало еще больше переменных со знаком `$`. Это переменные [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/variables/). Рекомендуется самостоятельно изучить какие они бывают и какие использовались для описания нашего конвейера.

И давайте зафиксируем какое-нибудь изменение в нашей программе в какой-нибудь новой ветке, чтобы потом эту ветку слить в ветку по умолчанию.

:::caution

Создание запроса на слияние отдается на самостоятельное выполнение. Полистайте предыдущие статьи данного руководства.

:::

Если вы все успешно сделаете, то увидите такой интерфейс запроса на слияние:

![Отчет о тестировании в запросе на слияние](images/merge-request-with-test-report-1.png)

Давайте, наконец-то, починим нашу программу:

```python
'''Программа расчета величин для прямоугольного треугольника.'''
import math


def get_hypotenuse(side_1, side_2):
    '''Рассчитать гипотенузу.'''
    return math.sqrt(math.pow(side_1, 2) + math.pow(side_2, 2))


def get_area(side_1, side_2):
    '''Получить площадь прямоугольного треугольника.'''
    return side_1 * side_2 / 2


if __name__ == "__main__":
    print("Введите a:")
    a = int(input())
    print("Введите b:")
    b = int(input())
    print("c =", get_hypotenuse(a, b))
    print("S =", get_area(a, b))

```

И теперь у нас в запросе на слияние все хорошо:

![Запрос на слияние с успешно завершенным конвейером](images/merge-request-with-test-report-2.png)

Полный отчет о тестировании для конвейера выглядит так:

![Полный отчет о тестировании для конвейера](images/pipeline-full-tests-report.png)

Нам остается только принять запрос на слияние и осуществить слияние. Что и предлагается вам сделать.
