---
sidebar_position: 3
---

# Добавление pytest

Платформа [pytest](https://docs.pytest.org/en/7.1.x/index.html) упрощает написание небольших, удобочитаемых тестов и может масштабироваться для поддержки сложного функционального тестирования приложений и библиотек.

Установить pytest:

```bash
pip install pytest
```

Удалим из репозитория `calculator_tests.py`. Вместо него создадим папку `tests`. В ней создаем пустой файл `__init__.py`.

Также файл с непосредственно тестом `test_calculator.py`:

```python
from calculator import get_area, get_hypotenuse


def test_get_hypotenuse_case_1():
    res = get_hypotenuse(3, 4)
    assert res == 5


def test_get_hypotenuse_case_2():
    res = get_hypotenuse(5, 12)
    assert res == 13


def test_get_hypotenuse_case_3():
    res = get_hypotenuse(8, 15)
    assert res ==  17


def test_get_area_case_1():
    res = get_area(3, 4)
    assert res ==  6


def test_get_area_case_2():
    res = get_area(5, 12)
    assert res == 30


def test_get_area_case_3():
    res = get_area(8, 15)
    assert res == 60

```

Не забудем сделать

```bash
pip freeze > requirements.txt
```

Файл `requirements.txt` будет выглядеть следующим образом:

```bash
altgraph==0.17.3
astroid==2.12.12
attrs==22.1.0
autopep8==1.7.0
dill==0.3.5.1
iniconfig==1.1.1
isort==5.10.1
lazy-object-proxy==1.7.1
macholib==1.16.2
mccabe==0.7.0
packaging==21.3
platformdirs==2.5.2
pluggy==1.0.0
py==1.11.0
pycodestyle==2.9.1
pyinstaller==5.5
pyinstaller-hooks-contrib==2022.10
pylint==2.15.4
pyparsing==3.0.9
pytest==7.1.3
toml==0.10.2
tomli==2.0.1
tomlkit==0.11.5
wrapt==1.14.1
```

В итоге репозиторий выглядеть должен следующим образом:

```bash
variant-X-user-Y
├── calculator.py
├── README.md
├── .gitignore
├── requirements.txt
└── tests
    ├── __init__.py
    └── test_calculator.py
```

:::danger

Не забудьте зафиксировать изменения и отправить их в удаленный репозиторий!

:::
