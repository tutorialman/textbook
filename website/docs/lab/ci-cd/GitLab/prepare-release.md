---
sidebar_position: 6
---

# Подготовка релиза

Мы научились тестировать. Теперь, давайте научимся собирать и выпускать релизы!

Измененный `.gitlab-ci.yml` для выпуска релизов:

```yaml
# This file is a template, and might need editing before it works on your project.
# Official language image. Look for the different tagged releases at:
# https://hub.docker.com/r/library/python/tags/
image: "python:3.10.3"

variables:
  CI_SERVER_PORT: 443
  CI_SERVER_PROTOCOL: https
  CI_SERVER_URL: https://bmstu.codes

# команды для запуска в контейнере Docker перед запуском каждого задания.
before_script:
  - python --version
  - pip install -r requirements.txt

# различные этапы в конвейере
stages:
  - Static Analysis
  - Test
  - Prepare Executable
  - Release

# определяет задание в статическом анализе
pylint:
  stage: Static Analysis
  rules:
    - if: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH
  script:
    - pylint -d C0301 calculator.py

pytest:
  stage: Test
  rules:
  - if: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH
  script:
    - pytest ./tests/*py --junitxml=${CI_PROJECT_DIR}/rspec.xml -s
  artifacts:
    when: always
    paths:
      - rspec.xml
    reports:
      junit: rspec.xml


prepare-executable:
  stage: Prepare Executable
  rules:
  - if: $CI_COMMIT_TAG
    when: never                                  # Не выполняется это задание, когда тег создается вручную
  - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH  # Запускает это задание, когда коммиты будут перенесены или объединены в ветку по умолчанию
  script:
  - echo GE_JOB_ID=$CI_JOB_ID >> build.env
  - pyinstaller calculator.py
  - apt update
  - apt install -y zip
  - zip -r executable.zip ./dist
  artifacts:
    paths:
      - executable.zip
    reports:
      dotenv: build.env                                      # используется artifacts:reports:dotenv, чтобы предоставить переменные другим заданиям

release:
  stage: Release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - job: prepare-executable
      artifacts: true
  before_script:
    - ''
  rules:
    - if: $CI_COMMIT_TAG
      when: never                                  # Не выполняется это задание, когда тег создается вручную
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH  # Запускает это задание, когда коммиты будут перенесены или объединены в ветку по умолчанию
  script:
    - echo "running release_job for $TAG"
    - echo 'Previous Job ID is printed below'
    - echo $GE_JOB_ID
  release:                                         # См. https://docs.gitlab.com/ee/ci/yaml/#release для поиска информации о доступных свойствах
    tag_name: 'v0.$CI_PIPELINE_IID'                # Версия увеличивается для каждого конвейера.
    description: 'v0.$CI_PIPELINE_IID'
    ref: '$CI_COMMIT_SHA'                          # Тег создается из SHA конвейера.
    assets:
      links:
        - name: 'Executable'
          url: '${CI_PROJECT_URL}/-/jobs/${GE_JOB_ID}/artifacts/file/executable.zip'
```

:::danger

Использование `variables` в начале файла:

```yaml
variables:
  CI_SERVER_PORT: 443
  CI_SERVER_PROTOCOL: https
  CI_SERVER_URL: https://bmstu.codes

```

является спецификой конкретно этой платформы. На платформе криво настроен `GitLab` и `Https`. В примере [gitlab.com/iu5edu/git-ci-cd](https://gitlab.com/iu5edu/git-ci-cd/textbook/-/blob/main/.gitlab-ci.yml) явно не указываются переменные `CI_SERVER*`.

:::

В этом файле мы добавили новые этапы:

- `Prepare Executable`
- `Release`

В сборке исполняемого указаны правила запуска, которые позволяют правильно реагировать на события: мы хотим готовить релиз в тот момент, когда изменения происходят с веткой по умолчанию. В этом же задании собираем из папки `./dist` архив `executable.zip`. Также мы дополнительно проводим некоторые операции, чтобы использовать некоторые переменные окружения в следующей задаче. В завершении описания задачи указано то, что  `executable.zip` является артефактом задания, который нужно сохранить.

Про выпуск релизов имеется [статья](https://docs.gitlab.com/ee/user/project/releases/) в официальной документации. Обратим внимание на то, как связаны между собой задачи `Prepare Executable` и `Release`: через `needs`. Также можно увидеть, что записанные переменные окружения в `Prepare Executable` доступны при выполнении задачи `Release`.

:::danger

Конкретно для платформы [bmstu.codes](https://bmstu.codes) в релизах ссылку на архив `executable.zip` вы не увидите. Это косяк платформы. На других версиях GitLab все работает (убедитесь сами [gitlab.com/iu5edu/git-ci-cd](https://gitlab.com/iu5edu/git-ci-cd/textbook/-/releases)).

:::

## Результаты

Давайте посмотрим на результаты.

В артефактах конвейера можно увидеть наш архив:

![Артефакты конвейера](images/prepare-release-result-1.png)

Релизы также появились:

![Релизы](images/prepare-release-result-2.png)

На этом все:

![На этом все](images/final.jpg)
