---
sidebar_position: 2
---

# Настройка Gitlab CI для приложения Python

:::info

Основано на статье ["Setting Gitlab CI/CD for Python application"](https://medium.com/cubemail88/setting-gitlab-ci-cd-for-python-application-b59f1fb70efe) и ["Python Unit Test And Some CI via GitLab (Python)"](https://geektechstuff.com/2021/04/05/python-unit-test-and-some-ci-via-gitlab-python/).

:::

В этой части работы предстоит настроить процесс GitLab CI для заданий:

- статический анализ с использованием [pylint](https://www.pylint.org/);
- модульное тестирование с использованием [unittest](https://docs.python.org/3/library/unittest.html).

## Создание `.gitlab-ci.yml`

Первым шагом является добавление в репозиторий `.gitab-ci.yml` yml определяет структуру и порядок конвейеров и определяет, как выполнять Gitlab Runner:

```yaml
# This file is a template, and might need editing before it works on your project.
# Official language image. Look for the different tagged releases at:
# https://hub.docker.com/r/library/python/tags/
image: "python:3.10.3" 
# команды для запуска в контейнере Docker перед запуском каждого задания.
before_script:
 - python --version
 - pip install -r requirements.txt
# различные этапы в конвейере
stages:
 - Static Analysis
 - Test
# определяет задание в статическом анализе
pylint:
 stage: Static Analysis
 script:
 - pylint -d C0301 calculator.py
```

Напомним, что перед этим заданием мы специально добавляли `requirements.txt` в репозиторий.

Мониторинг выполнения можно найти в левой вкладке `CI / CD` в нашем проекте:

![Обзор мониторинга pipelines](images/pipelines-overview.png)

## Состояние выполнения задач

Перейдя на вкладку мониторинга конвейер, можно увидеть, что он завершился с ошибкой:

![Конвейер с ошибкой](images/pipeline-overview-failed.png)

Во вкладке "Неудачные задания" показывает причину, по которой задача с pylint завершилась с ошибкой:

![Обзор задач с ошибкой](images/pipeline-overview-failed-jobs.png)

## Внесение исправлений

После изучения перечня предупреждений, на которые указал pylint, программа `calculator.py` была изменена следующим образом:

```python
'''Программа расчета величин для прямоугольного треугольника.'''
import math


def get_hypotenuse(side_1, side_2):
    '''Рассчитать гипотенузу.'''
    return math.sqrt(math.pow(side_1, 3) + math.pow(side_2, 3))


def get_area(side_1, side_2):
    '''Получить площадь прямоугольного треугольника.'''
    return side_1 * side_2


if __name__ == "__main__":
    print("Введите a:")
    a = int(input())
    print("Введите b:")
    b = int(input())
    print("c =", get_hypotenuse(a, b))
    print("S =", get_area(a, b))

```

:::danger

Не забудьте зафиксировать изменения и отправить их в удаленный репозиторий!

:::

Изменения, внесенные в код, привели к положительному результату. Теперь задача завершается успешно:

![Исправление конвейера с задачей pylint](images/pipeline-fix-pylint.png)

Теперь задания выполняются, и мы можем перейти к следующему этапу тестирования.

## Запуск тестов с помощью pytest на GitLab CI

Добавим этап тестирования в `.gitlab-ci.yml`:

```yaml
# This file is a template, and might need editing before it works on your project.
# Official language image. Look for the different tagged releases at:
# https://hub.docker.com/r/library/python/tags/
image: "python:3.10.3"
# команды для запуска в контейнере Docker перед запуском каждого задания.
before_script:
  - python --version
  - pip install -r requirements.txt
# различные этапы в конвейере
stages:
  - Static Analysis
  - Test
# определяет задание в статическом анализе
pylint:
  stage: Static Analysis
  script:
    - pylint -d C0301 calculator.py

unittest:
  stage: Test
  script:
    - python -m unittest calculator_tests.py
```

unittest покажет название тестового примера и результат:

![Провал задачи unittest](images/pipeline-unittest-failed.png)

И да, эта задача успешно провалилась. Давайте начнем с того, что изменим наш тест. Нам нужно что-то посерьезнее. Например, [pytest](https://docs.pytest.org/en/7.1.x/).
