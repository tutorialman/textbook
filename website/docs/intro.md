---
sidebar_position: 1
---

# О руководстве

В данном руководстве содержится информация об основах работы с репозиториями и CI/CD.

## Мотивация

Почему это руководство появилось на свет?

![Мотивация мем](images/motivation-meme.jpeg)

Потому, что имеются хорошие/полезные/необходимые/`подставить_свое` инструменты для студента, но их боятся/игнорируют/обходят/`подставить_свое` 😢. Они могут пригодится для написания программы/статьи/диплома/`подставить_свое`.

Данное руководство призвано познакомить студента с основами работы с репозиториями и CI/CD.
